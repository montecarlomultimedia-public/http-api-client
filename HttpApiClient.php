<?php
namespace Montecarlomultimedia\Public\HttpApiClient;

use DateInterval;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HttpApiClient {
    static int $timeout = 10;

    private HttpClientInterface $client;
    private CacheInterface $cacheApi;
    private CacheInterface $cacheFallback;

    public function __construct(
        HttpClientInterface $client,
        CacheInterface      $cacheApi, CacheInterface $cacheFallback
    )
    {
        $this->client = $client;
        $this->cacheApi = $cacheApi;
        $this->cacheFallback = $cacheFallback;
    }


    /**
     * @param string $method GET|POST
     * @param string $url Api Url
     * @param array|null $options options
     * @param string|null|bool $expiresAfter cache delay
     * @param bool|null $refresh
     * @param CacheInterface|bool|null $cacheApi Cache pool to use (or false for no cache)
     * @param CacheInterface|bool|null $cacheFallback Cache pool to use for fallback (or false for no cache)
     * @param bool|null $throw Throws an exception if not available
     * @return mixed
     */
    public function request(
        string          $method, string $url, ?array $options = [], mixed $expiresAfter = '3 hours', ?bool $refresh = false,
        mixed $cacheApi = null, mixed $cacheFallback = null, ?bool $throw = true
    ): mixed
    {
        static $apiExecutionCache = [];

        if (!isset($options['timeout'])) $options['timeout'] = self::$timeout;

        $item = null;
        $cache_key = md5($method.$url.serialize($options).$expiresAfter);
        $cache_key_inf = $cache_key.'_inf';
        if (isset($apiExecutionCache[$cache_key])) return $apiExecutionCache[$cache_key];

        $cacheApi = is_null($cacheApi)?$this->cacheApi:$cacheApi;
        $cacheFallback = is_null($cacheFallback)?$this->cacheFallback:$cacheFallback;

        if (false!==$cacheApi) {
            try {
                if (false !== $expiresAfter && ($item = $cacheApi->getItem($cache_key)) && $item->isHit()) {
                    $result = $item->get();
                    if (!$refresh) {
                        if (is_array($result)) {
                            if (empty($result['cache'])) $result['cache'] = [];
                            $result['cache']['hit'] = 1;
                        }
                        return $result;
                    }
                }
            } catch (InvalidArgumentException) {
            }
        }
//        $b = $url; $url = 'http://192.168.56.3';
        try {
            $req = $this->client->request($method, $url, $options);
            $result = $req->toArray();
        } catch (TransportExceptionInterface|DecodingExceptionInterface|RedirectionExceptionInterface|ClientExceptionInterface|ServerExceptionInterface $e) {
            if ($e->getCode()==404) return null;
            self::$timeout = self::$timeout>3?self::$timeout--:self::$timeout;
            if (false!==$cacheFallback) {
                $itemInf = $cacheFallback->getItem($cache_key_inf);
                if ($itemInf && $itemInf->get()) {
                    $result = $itemInf->get();
                    if (is_array($result)) {
                        if (empty($result['cache'])) $result['cache'] = [];
                        $result['cache']['fallback'] = 1;
                    }
                    $apiExecutionCache[$cache_key] = $result;
                    return $result;
                }
            }
            if (!$throw) return null;
            throw new ServiceUnavailableHttpException(60, 'API not available');
        }


        if (false!==$cacheApi && $item && !is_null($result)) {
            if (is_array($result)) {
                $result['cache'] = [
                    'time' => date(DATE_RFC2822),
                    'expiresAfter' => $expiresAfter,
                ];
            }
            $item->set($result);
            $item->expiresAfter(DateInterval::createFromDateString($expiresAfter));
            $cacheApi->save($item);

            if (false!==$cacheFallback) {
                $itemInf = $cacheFallback->getItem($cache_key_inf);
                $itemInf->set($result);
                $cacheFallback->save($itemInf);
            }
        }
        $apiExecutionCache[$cache_key] = $result;

        if (is_array($result)) {
            if (!is_array($result['cache'])) $result['cache'] = [];
            $result['cache']['live'] = true;
        }
        return $result;
    }
}
